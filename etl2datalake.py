#!/usr/local/bin/python
import sys
import logging
from etl2blob import load_config
from azure.storage.blob import BlobClient
from azure.storage.filedatalake import DataLakeServiceClient


logging.basicConfig(stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    
logger = logging.getLogger(__name__)

config = load_config()

def extract_blob():
    logger.info('LOGING INTO BLOB...')
    blob = BlobClient(
        account_url="https://"+ config["AZURE_STORAGE_ACCOUNT_NAME"] +".blob.core.windows.net",
        container_name=config["AZURE_CONTAINER_NAME"],
        blob_name="covid_data.csv",
        credential=config["AZURE_ACCOUNT_KEY"]
    )
    logger.info('EXTRACTING...')
    with open("blob_covid.csv", "wb") as file:
        data = blob.download_blob()
        data.readinto(file)

def initialize_storage_account(storage_account_name, storage_account_key):
    try:
        global service_client

        service_client = DataLakeServiceClient(account_url="https://"+ storage_account_name +".dfs.core.windows.net", credential=storage_account_key)

    except Exception as e:
        logger.info('EXCEPTION...')
        logger.info(e)

def create_file_system():
    try:
        global file_system_client
        logger.info('CREATING A CONTAINER NAMED AZDL-COVID22')
        file_system_client = service_client.create_file_system(file_system=config["AZDL_CONTAINER_NAME"])
    
    except Exception as e:
        logger.info('EXCEPTION...')
        logger.info(e)

def create_directory():
    try:
        logger.info('CREATING A DIRECTORY NAMED DIR-COVID22')
        file_system_client.create_directory("dir-covid22")
    
    except Exception as e:
        logger.info('EXCEPTION...')
        logger.info(e)

def upload_file_to_container_datalake(local_file):
    try:
        logger.info('UPLOADING FILE TO DATA LAKE STORAGE...')
        file_system_client = service_client.get_file_system_client(file_system=config["AZDL_CONTAINER_NAME"])
        directory_client = file_system_client.get_directory_client("dir-covid22")

        file_client = directory_client.get_file_client("uploaded-covid.csv")

        with open(local_file, "rb") as data:
            file_client.upload_data(data, overwrite=True)
            logger.info('UPLOADED TO AZURE DATA LAKE')


    except Exception as e:
        logger.info('EXCEPTION...')
        logger.info(e)


if __name__ == "__main__":
    extract_blob()
    initialize_storage_account(config["AZDL_STORAGE_ACCOUNT_NAME"], config["AZDL_ACCOUNT_KEY"])
    create_file_system()
    create_directory()
    upload_file_to_container_datalake('blob_covid.csv')