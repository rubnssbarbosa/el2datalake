#!/usr/local/bin/python
import os
import sys
import yaml
import logging
import requests
import pandas as pd
from datetime import datetime
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

logging.basicConfig(stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    
logger = logging.getLogger(__name__)


def extract():
    logger.info('EXTRACTING...')
    today = datetime.today().strftime('%Y-%m-%d')
    #print(today)
    #extract data from API
    hoje = '2022-01-23'
    url = 'https://indicadores.integrasus.saude.ce.gov.br/api/casos-coronavirus?dataInicio='+ hoje +'&dataFim=' + hoje
    req = requests.get(url)
    data = req.json()
    
    if data:
        df = pd.DataFrame(data)
        df.to_csv('covid_data.csv', mode='a', header=False)
    else:
        logger.info('THERE IS NOT DATA')

def load_config():
    directory = os.path.dirname(os.path.abspath(__file__))
    with open(directory + "/config.yaml", "r") as yamlfile:
        return yaml.load(yamlfile, Loader=yaml.FullLoader)

def get_file(directory):
    with os.scandir(directory) as entries:
        for entry in entries:
            if entry.is_file() and not entry.name.startswith('.'):
                yield entry

def upload(local_file, connection_string, container_name):
    try:
        
        container_client = ContainerClient.from_connection_string(connection_string, container_name)
        logger.info('UPLOAD FILE TO BLOB STORAGE...')
        blob_client = container_client.get_blob_client(local_file)
        with open(local_file, "rb") as data:
            blob_client.upload_blob(data)
            logger.info('UPLOADED TO BLOB STORAGE')

    except Exception as ex:
        logger.info('EXCEPTION...')
        logger.info(ex)


if __name__ == "__main__":

    extract()
    config = load_config()
    upload("covid_data.csv", config["AZURE_STORAGE_CONNECTION_STRING"], config["AZURE_CONTAINER_NAME"]) 
    